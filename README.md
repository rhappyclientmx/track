# TrackStreet Tech Challenge

Tech challenge of 4 questions for track street

## Getting Started

run app.rb to run all the exercise files

```
ruby app.rb
```

You can comment each line of p or puts to run individual exercises

### Prerequisites

```
ruby 2.3.7 or newer
```

### Install ruby if needed

```
brew install rbenv
rbenv init
rbenv install 2.3.7
```

### Tests

Execute in your console

```
ruby -Itest test/tests.rb
```
