# Write a program to compute the frequency of the words from the input. The output
# should output after sorting the key alphanumerically.
# Suppose the following input is supplied to the program:
# New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python
# 3.
# Then, the output should be:
# 2:2
# 3.:1
# 3?:1
# New:1
# Python:5
# Read:1
# and:1
# between:1
# choosing:1
# or:2
# to:1
class FrequencyOfWords
  attr_reader :text

  def initialize(text)
    @text = text
  end

  def calculate
    counter = Hash.new(0)
    text_words.each do |word|
      counter[word] += 1
    end
    merge!(counter)
  end

  private

  def text_words
    text.split
  end

  def merge!(counter)
    counter.sort.map! { |h| h.join ':' }.join("\n")
  end
end
