# Write a program that accepts a comma-separated sequence of words as input and
# prints the words in a comma-separated sequence after sorting them alphabetically.
class SortAlpha
  attr_reader :sequence

  def initialize(sequence)
    @sequence = sequence
  end

  def ordered
    sequence.sort
  end
end

# Same result but with a string instead of an array as sequence as i didnt
# understood the instructions
class StringSort
  attr_reader :words

  def initialize(words)
    @words = words
  end

  def ordered
    words.split(',').sort.join(', ')
  end
end
