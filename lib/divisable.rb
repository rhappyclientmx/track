# Write a program which will find all such numbers which are divisible by 7,
# but are not a multiple of 5, and are between 2000 and 3200 (both included).
# The numbers obtained should be printed in a comma-separated sequence on a single line.
class Divisable
  MIN = 2000
  MAX = 3200

  class << self
    def call
      (MIN..MAX).select { |num| valid?(num) }
    end

    private

    def valid?(num)
      !div_by_5?(num) && div_by_7?(num)
    end

    def div_by_5?(num)
      (num % 5).zero?
    end

    def div_by_7?(num)
      (num % 7).zero?
    end
  end
end
