# Write a program that calculates and prints the value according to the given formula:
# Q = Square root of [(2 * C * D)/H]
# The following are the fixed values of C and H:
# C is 50. H is 30.
# D is the variable whose values should be input to your program in a comma-separated sequence.
# Example
# Let us assume the following comma-separated input sequence is given to the program:  100,150,180
# The output of the program should be: 18,22,24
require 'cmath'
class SquareRoot
  attr_reader :sequence, :nums

  C = 50
  H = 30

  def initialize(sequence)
    @sequence = sequence
    @nums = []
  end

  def calculate
    sequence.each do |num|
      nums << flatten!(num)
    end
    nums
  end

  private

  def flatten!(num)
    operation(num).floor
  end

  def operation(num)
    CMath.sqrt(divide(num))
  end

  def divide(num)
    multiply(num) / H
  end

  def multiply(num)
    (2 * C * num)
  end
end
