require_relative 'lib/divisable'
require_relative 'lib/square_root'
require_relative 'lib/sort_alpha'
require_relative 'lib/frequency_of_words'

# Initialize
class TrackStreet
  NUMS = [100, 150, 180].freeze
  TEXT = 'New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.'.freeze
  WORDS = %w[without hello bag world].freeze
  WORDS_STRING = 'without,hello,bag,world'.freeze

  def self.start!
    # Execute ex1
    p Divisable.call
    # Execute ex2
    puts '----- EX2 ------'
    p SquareRoot.new(NUMS).calculate
    # Execute ex3
    puts '----- EX3 ------'
    p SortAlpha.new(WORDS).ordered
    p StringSort.new(WORDS_STRING).ordered
    # Execute ex4
    puts '----- EX4 ------'
    puts FrequencyOfWords.new(TEXT).calculate
  end
end
