require 'test/unit'
require './lib/square_root'

class SquareRootTest < Test::Unit::TestCase
  NUMS = [100, 150, 180].freeze
  RESULT = [18, 22, 24].freeze

  def test_world
    assert_equal RESULT, SquareRoot.new(NUMS).calculate, 'should return the expected ordered numbers'
  end
end
