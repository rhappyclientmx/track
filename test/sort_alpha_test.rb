require 'test/unit'
require './lib/sort_alpha'

class SortAlphaTest < Test::Unit::TestCase
  WORDS = %w[without hello bag world].freeze
  RESULT = %w[bag hello without world].freeze

  def test_world
    assert_equal RESULT, SortAlpha.new(WORDS).ordered, 'should return the expected ordered strings'
  end
end
