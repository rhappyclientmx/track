require 'test/unit'
require './lib/frequency_of_words'

class FrequencyOfWordsTest < Test::Unit::TestCase
  TEXT = 'New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.'.freeze
  RESULT = "2:2\n3.:1\n3?:1\nNew:1\nPython:5\nRead:1\nand:1\nbetween:1\nchoosing:1\nor:2\nto:1".freeze

  def test_world
    assert_equal RESULT, FrequencyOfWords.new(TEXT).calculate, 'should return the expected frequency of strings'
  end
end
